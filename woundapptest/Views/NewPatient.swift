//
//  NewPatient.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 31/12/2021.
//

import SwiftUI
import Combine

struct NewPatient: View {
    @ObservedObject private var viewmodel = NewPatientVM()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @FocusState var isInputActive: Bool
    
    var body: some View {
        VStack {
            ScrollView(showsIndicators: false) {
                //TODO: image of the patient
                Circle().foregroundColor(Color(.systemGray6)).frame(width: 100, height: 100).padding(.bottom,40)
                
                
                //input fields
                VStack (alignment:.leading){
                    HStack{
                        TextField("*First Name", text: $viewmodel.firstName)
                            .frame(width: 140, height: 15, alignment: .center)
                            .padding()
                            .background(Color(.systemGray6))
                            .cornerRadius(5.0)
                            .focused($isInputActive)
                        TextField("*Last Name", text: $viewmodel.lastName)
                            .frame(width: 140, height: 15, alignment: .center)
                            .padding()
                            .background(Color(.systemGray6))
                            .cornerRadius(5.0)
                            .focused($isInputActive)
                    }.padding(.bottom, 20)
                    
                    HStack{
                        DatePicker("*Date of Birth", selection: $viewmodel.bd, displayedComponents: .date)
                            .foregroundColor(.gray)
                            .frame(width: 320, height: 15, alignment: .center)
                            .padding()
                            .background(Color(.systemGray6))
                            .cornerRadius(5.0)
                            .focused($isInputActive)
                    }.padding(.bottom,20)
                    
                    HStack {
                        TextField("*Sex", text: $viewmodel.sex)
                            .frame(width: 140, height: 15, alignment: .center)
                            .padding()
                            .background(Color(.systemGray6))
                            .cornerRadius(5.0)
                            .focused($isInputActive)
                        TextField("*Height", text: $viewmodel.height)
                            .frame(width: 140, height: 15, alignment: .center)
                            .padding()
                            .background(Color(.systemGray6))
                            .cornerRadius(5.0)
                            .focused($isInputActive)
                            .keyboardType(.numberPad)
                    }
                    
                    HStack{
                        TextField("*Weight", text: $viewmodel.weight)
                            .frame(width: 140, height: 15, alignment: .center)
                            .padding()
                            .background(Color(.systemGray6))
                            .cornerRadius(5.0)
                            .focused($isInputActive)
                            .keyboardType(.numberPad)
                    }.padding(.bottom,20)
                    
                    HStack{
                        TextField("*Address", text: $viewmodel.address)
                            .frame(width: 140, height: 15, alignment: .center)
                            .padding()
                            .background(Color(.systemGray6))
                            .cornerRadius(5.0)
                            .focused($isInputActive)
                        TextField("*Phone number", text: $viewmodel.phone)
                            .frame(width: 140, height: 15, alignment: .center)
                            .padding()
                            .background(Color(.systemGray6))
                            .cornerRadius(5.0)
                            .focused($isInputActive)
                            .keyboardType(.numberPad)
                    }
                    HStack {
                        TextField("*E-Mail", text: $viewmodel.email)
                            .frame(width: 140, height: 15, alignment: .center)
                            .padding()
                            .background(Color(.systemGray6))
                            .cornerRadius(5.0)
                            .focused($isInputActive)
                    }.padding(.bottom,20)
                    
                    TextField("*MRN", text: $viewmodel.mrn)
                        .frame(width: 140, height: 15, alignment: .leading)
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(5.0)
                        .focused($isInputActive)
                        .keyboardType(.numberPad)
                    
                }.padding(.bottom,80)
            }
            
            // buttons at the bottom of the screen
            HStack(spacing:20){
                Group {
                    Button {
                        viewmodel.clearAll()
                    } label: {
                        Text("Clear All").foregroundColor(Color("Icon_Patients")).frame(width: 100, height: 10, alignment: .center).padding().overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke(Color("Icon_Patients"), lineWidth: 1)
                        )
                    }
                }.background(.white).cornerRadius(5).padding(5)
                Group {
                    Button {
                        if viewmodel.save() { presentationMode.wrappedValue.dismiss() }
                    } label: {
                        Text("Save").foregroundColor(.white).frame(width: 100, height: 10, alignment: .center).padding()
                    }
                }.background(Color("Icon_Patients")).cornerRadius(5).padding(5)
            }
        }
        .padding(.horizontal)
        .ignoresSafeArea(.keyboard, edges: .bottom)
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                Spacer()
                Button("Done") {
                    isInputActive = false
                }
            }
        }
        .alert("Empty Fields", isPresented: $viewmodel.showingAlert) {
            // just the "OK" button
        } message: {
            Text(viewmodel.alertText)
        }
    }
}

struct NewPatient_Previews: PreviewProvider {
    static var previews: some View {
        NewPatient()
    }
}
