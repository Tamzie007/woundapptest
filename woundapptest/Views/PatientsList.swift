//
//  PatientsList.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 16/01/2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct PatientsList: View {
    
    @ObservedObject private var viewmodel = PatientsListVM()
    
    var body: some View {
        List{
            ForEach(viewmodel.patients){ patient in
                NavigationLink(destination: PatientProfile(patient:patient), label: {
                        HStack {
                            WebImage(url:viewmodel.url)
                                .resizable()
                                .frame(width: 50, height: 50, alignment: .leading) 
                                .clipShape(Circle())
                            VStack(alignment:.leading) {
                            HStack {
                                Text(patient.firstName)
                                Text(patient.lastName)
                                //Text(patient.id ?? "")
                                //NavigationLink(destination: HomeScreen(), label:{ Text("GO")})
                            }.font(.title2)
            
                            Text(patient.gender)
                            }
                        }})
        
                }
        }.listStyle(.plain)
            .onAppear(){
                self.viewmodel.fetchData()
                self.viewmodel.fetchImage(id:"RIqYrfgzwV6wrVinicyH")
            }
            
    }
}

struct PatientsList_Previews: PreviewProvider {
    static var previews: some View {
        PatientsList()
            .environmentObject(PatientsListVM())
    }
}
