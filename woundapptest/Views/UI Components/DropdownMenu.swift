//
//  DropdownMenu.swift
//  woundapptest
//
//  Created by Carlos Morales III on 3/22/22.
//

import SwiftUI

struct DropdownMenu: View {
	@State private var expand: Bool = false
	@State private var currentSelection: String = "Select Option"
	@ObservedObject var dropdownMenuVM = DropdownMenuViewModel()
	
	let cellHeight: CGFloat = 35
	let containerWidth: CGFloat = 172.05
	let containerHeight: CGFloat = 173
	let cornerRadius: CGFloat = 10
	
    var body: some View {
		VStack {
			VStack(spacing: 0) {
				HStack {
					Text(currentSelection)
						.foregroundColor(.black)
					Image(systemName: expand ? "chevron.up" : "chevron.down")
				}
				.onTapGesture {
					self.expand.toggle()
				}
				.frame(width: containerWidth, height: cellHeight, alignment: .center)
				.overlay(
					RoundedRectangle(cornerRadius: cornerRadius)
						.stroke()
						.foregroundColor(.blue)
				)
				.position(x: containerWidth / 2, y: 19)
				
				if expand {
					List {
						ForEach(dropdownMenuVM.subRegions, id: \.self) { subRegion in
							Button(
								action: {
									currentSelection = subRegion
									expand = false
								},
								label: {
									Text(subRegion)
										.foregroundColor(.black)
								})
						}
					}
					.frame(width: containerWidth, height: containerHeight - 35, alignment: .center)
					.listStyle(.inset)
					.position(x: containerWidth / 2, y: 20)
				}
			}
		}
		.frame(width: containerWidth, height: containerHeight, alignment: .center)
    }
}

struct DropdownMenu_Previews: PreviewProvider {
    static var previews: some View {
		DropdownMenu()
    }
}
