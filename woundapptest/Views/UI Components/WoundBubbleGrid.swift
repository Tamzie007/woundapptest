//
//  WoundBubbleGrid.swift
//  woundapptest
//
//  Created by Brady Robshaw on 1/19/23.
//

import SwiftUI

struct WoundBubbleGrid: View {
    let wounds: [Wound]
    private let items: [GridItem] = Array(repeating: GridItem(), count: 2)
    
    var body: some View {
        ScrollView( .horizontal, showsIndicators: false) {
            LazyHGrid(rows: items) {
                ForEach(wounds, id: \.id) { wound in
                    WoundDescriptionBubble(wound: wound)
                }
            }
        }
        .frame(minHeight: 200, maxHeight: 200)
    }
}

struct WoundBubbleGrid_Previews: PreviewProvider {
    static var previews: some View {
        WoundBubbleGrid(wounds: [Wound.example])
    }
}
