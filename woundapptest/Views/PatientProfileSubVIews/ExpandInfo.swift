//
//  ExpandInfo.swift
//  woundapptest
//
//  Created by stella on 3/3/22.
//

import SwiftUI

struct ExpandInfo<Content: View>: View {
    
    @State var expanding: Bool  = false
    let information:String
    @ViewBuilder let content: Content
    
    var body: some View {
        VStack {
            HStack{
                Image(systemName: expanding ? "chevron.up":"chevron.down")
                Text(information).font(.system(size: 20))
            }.frame(maxWidth: .infinity, alignment: .leading).padding(.horizontal)
            
            Divider()
            
            if expanding {
                content
            }
        }
        .onTapGesture {
            expanding.toggle()
        }
    }
}

struct ExpandInfo_Previews: PreviewProvider {
    static var previews: some View {
        ExpandInfo(information: "Contact Details") {
            ContactDetails(patient: Patients.testData[0])
        }
    }
}
