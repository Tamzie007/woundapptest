//
//  Profile.swift
//  woundapptest
//
//  Created by stella on 3/3/22.
//

import SwiftUI

struct Profile: View {
    
    var patient:Patients
    
    var body: some View {
        VStack{
            HStack(spacing:40){
                PatientPicturePicker(patient: patient)
                VStack(alignment: .leading){
                    Text(patient.firstName + " " + patient.lastName)
                        .font(.largeTitle)
                    Text("D.O.B.: " + patient.birthDateString)
                    Text("Age: " + "22")
                    Text("Gender: " + patient.gender)
                    Text("MRN: " + patient.MRNString)
                }
            }
            HStack{
                Spacer()
                Group {
                    Button {
                        
                    } label: {
                        Image(systemName:"pencil").foregroundColor(.white)
                            .padding(.leading, 5)
                        Text("edit")
                            .padding(.trailing, 5)
                            .foregroundColor(.white)
                            .padding(5)
                    }.background(Color("Icon_Patients"))
                        .cornerRadius(5)
                        .padding()
                }
            }
        }
    }
}

struct Profile_Previews: PreviewProvider {
    static var previews: some View {
        Profile(patient: Patients.example)
            .environmentObject(SaveImagesVM())
    }
}
