//
//  PatientHistory.swift
//  woundapptest
//
//  Created by stella on 3/4/22.
//

import SwiftUI

struct PatientHistory: View {
    
    var patient:Patients
    
    var body: some View {
        VStack {
            Group {
                Button {
                    
                } label: {
                    Text("Details").foregroundColor(.white)
            }
            }.padding(10).background(Color("Icon_Scanwound")).cornerRadius(5).padding(.vertical).frame(maxWidth: .infinity, alignment: .trailing).padding(.horizontal)
            HStack(spacing:40) {
                VStack(alignment: .leading) {
                    Text("Health History").foregroundColor(Color("Tile Text"))
                    Group {
                        Text("Diabetes?")
                        Text("High Cholesterol?")
                        Text("High Blood Pressure?")
                        Text("Other conditions?")
                    }.frame(height: 30)
                    Text("Social History").foregroundColor(Color("Tile Text"))
                    Group{
                        Text("Smoker?")
                        Text("Vaping?")
                        Text("Drinking alcohol?")
                        Text("Drugs?")
                        Text("Physical active?")
                        Text("Martial status?")
                    }.frame(height: 30)
                }.fixedSize(horizontal: true, vertical: false)
                Divider()
                VStack (alignment:.leading){
                    Text(" ")
                    Group{
                        HStack{
                            Text(String(patient.patientHistory.diabetes))
                            Divider()
                            Text("Type 2")
                        }
                        Text(String(patient.patientHistory.highCholesterol))
                        Text(String(patient.patientHistory.highBloodPressure))
                        Text(String(patient.patientHistory.otherConditions))
                    }.frame(height: 30)
                    Text(" ")
                    Group{
                        Text(String(patient.patientHistory.smoker))
                        Text(String(patient.patientHistory.vaping))
                        Text(String(patient.patientHistory.drinkingAlcohol))
                        Text(String(patient.patientHistory.drugs))
                        Text(String(patient.patientHistory.physicalActivity))
                        Text(String(patient.patientHistory.maritalStatus))
                    }.frame(height: 30)
                }
            }.fixedSize().padding()
        }
    }
}

struct PatientHistory_Previews: PreviewProvider {
    static var previews: some View {
        PatientHistory(patient: Patients.example)
    }
}
