//
//  Wounds.swift
//  woundapptest
//
//  Created by stella on 3/2/22.
//

import SwiftUI

struct Wounds: View {
    @EnvironmentObject var viewModel: PatientWoundVM
    var patient:Patients
    
    @State private var newWoundTab = false
    
    var body: some View {
        VStack(alignment: .leading){
            HStack {
                Button {
                    newWoundTab = true
                } label: {
                    Group {
                        Text("Add New Wound").foregroundColor(.white).padding(10)
                    }
                    .background(Color("Icon_Scanwound"))
                    .cornerRadius(5).frame(maxWidth: .infinity, alignment: .trailing)
                }
            }.padding(.horizontal)
            
            WoundsList()
            
        }
        .padding()
        .sheet(isPresented: $newWoundTab) { AddNewWoundView() }
        .onAppear { viewModel.loadWounds()} // can comment out if you want the preview to work
    }
}

struct Wounds_Previews: PreviewProvider {
    static var previews: some View {
        let vm = PatientWoundVM(patient: Patients.example)
        vm.wounds = [Wound.example]
        
        return NavigationView {
            Wounds(patient: Patients.example)
                .environmentObject(vm)
        }
    }
}

struct WoundsList: View {
    @EnvironmentObject var viewModel: PatientWoundVM
    @State private var isWoundDetail: Bool = false
    
    var body: some View {
        ForEach (viewModel.wounds) { wound in
            VStack(spacing: 0) {
                HStack{
                    VStack(alignment:.leading){
                        Group{
                            Text(wound.name)
                                .bold()
                                .fixedSize()
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .font(.title3)
                            Text(wound.stage)
                            Text(wound.location)
                        }.frame(height: 12)
                    }
                    
                    Group{
                        Button {
                            viewModel.setDisplay(to: wound)
                            isWoundDetail = true
                        } label: {
                            Text("View")
                                .padding(5)
                                .padding(.horizontal)
                                .foregroundColor(Color("Icon_Scanwound"))
                                .overlay(
                                    RoundedRectangle(cornerRadius: 5)
                                        .stroke(Color("Icon_Scanwound"), lineWidth: 2)
                                )
                        }
                        
                        
                    }.frame(maxWidth: .infinity, alignment: .trailing)
                }
                
                NavigationLink("destination", destination: WoundDetailView().environmentObject(viewModel), isActive: $isWoundDetail)
                    .hidden()
                
                Divider()
            }.padding()
        }
    }
}
