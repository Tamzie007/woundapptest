//
//  WoundDetailView.swift
//  woundapptest
//
//  Created by Brady Robshaw on 1/23/23.
//

import SwiftUI

struct WoundDetailView: View {
    @EnvironmentObject var viewModel: PatientWoundVM
    @State private var newRecordTab = false
    
    var body: some View {
        VStack (alignment: .center, spacing: 0) {
            WoundDataView()
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    Button {
                        viewModel.showWoundRecord(nil)
                    } label: {
                        MediumButton(text: "General", fgcolor: "Primary", bgcolor: "Whght", width: 125, height: 45)
                    }
                    
                    NavigationLink {
                        ScanWound()
                    } label: {
                        MediumButton(text: "+", fgcolor: "Primary", bgcolor: "Whght", width: 50, height: 45)
                    }
                    
                    ForEach(viewModel.displayData.woundRecord) { record in
                        Button {
                            viewModel.showWoundRecord(record)
                        } label: {
                            MediumButton(text: "\(Date(timeIntervalSince1970: TimeInterval(record.UTC)).formatted(date: .abbreviated, time: .omitted))", fgcolor: "Primary", bgcolor: "Whght", width: 125, height: 45)
                        }

                        
                    }
                }
                .padding(.leading)
            }
            .frame(height: 45)
            .padding(.bottom)
            
            WoundDiagramView(location: viewModel.displayData.locationCode ?? .anteriorHead)
                .padding()
            
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("\(viewModel.patient.firstName) - \(viewModel.displayData.name)")
        .sheet(isPresented: $newRecordTab) {
            AddNewRecordView()
        }
    }
}

struct WoundDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let vm = PatientWoundVM(patient: Patients.example)
        vm.setDisplay(to: Wound.example)
        
        return NavigationView {
            NavigationView {
                WoundDetailView()
                    .environmentObject(vm)
            }
            
        }
        
    }
}
