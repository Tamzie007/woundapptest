//
//  WoundDiagramView.swift
//  woundapptest
//
//  Created by Brady Robshaw on 2/15/23.
//

import SwiftUI

struct WoundDiagramView: View {
    let location: LocationCode?
    
    @State private var toEdge: CGFloat = 0
    @State private var x: CGFloat = 0
    @State private var y: CGFloat = 0
    
    var body: some View {
        GeometryReader { geo in
            ZStack(alignment: .center) {
                Image("Wound Diagram")
                    .resizable()
                    .scaledToFit()
                
                if location != nil {
                    Circle()
                        .frame(width: 15)
                        .foregroundColor(.red)
                        .offset(x: x, y: y)
                }
            }
            .frame(width: toEdge * 2, height: toEdge * 2)
            .position(x: geo.frame(in: .local).midX, y: geo.frame(in: .local).midY)
            .onAppear {
                findSmallSize(geo: geo)
                setXandY()
            }
        }
    }
}

struct WoundDiagramView_Previews: PreviewProvider {
    @State var location: LocationCode = .posteriorHead
    
    static var previews: some View {
        WoundDiagramView(location: .posteriorHead)
    }
}

extension WoundDiagramView {
    func findSmallSize(geo: GeometryProxy) {
        if geo.size.width > geo.size.height {
            toEdge = geo.size.height / 2
        } else {
            toEdge = geo.size.width / 2
        }
    }
    
    func setXandY() {
        guard let location = location else { return }
        x = toEdge * location.ofsets.x * 0.95
        y = toEdge * location.ofsets.y * 0.95
    }
}

extension LocationCode {
    var ofsets: (x: CGFloat, y: CGFloat) {
        switch self {
        case .anteriorHead:
            return (-0.72,-0.8)
        case .posteriorHead:
            return (-0.13,-0.8)
        case .anteriorRightChest:
            return (-0.19,-0.46)
        case .anteriorLeftChest:
            return (-0.07,-0.46)
        case .posteriorRightChest:
            return (-0.64,-0.46)
        case .posteriorLeftChest:
            return (-0.75,-0.46)
        case .anteriorRightAbdomen:
            return (-0.19,-0.24)
        case .anteriorLeftAbdomen:
            return (-0.07,-0.24)
        case .posteriorRightAbdomen:
            return (-0.64,-0.24)
        case .posteriorLeftAbdomen:
            return (-0.75,-0.24)
        case .gluteusRight:
            return (-0.62,-0.06)
        case .gluteusLeft:
            return (-0.75,-0.06)
        case .posteriorRightProximalArm:
            return (-0.51,-0.42)
        case .posteriorLeftProximalArm:
            return (-0.87,-0.42)
        case .anteriorRightProximalArm:
            return (-0.3,-0.46)
        case .anteriorLeftProximalArm:
            return (0.05,-0.46)
        case .posteriorRightDistalArm:
            return (-0.48,-0.15)
        case .posteriorLeftDistalArm:
            return (-0.9,-0.15)
        case .anteriorRightDistalArm:
            return (-0.33,-0.15)
        case .anteriorLeftDistalArm:
            return (0.08,-0.15)
        case .posteriorRightProximalLeg:
            return (-0.6,0.1)
        case .posteriorLeftProximalLeg:
            return (-0.77,0.1)
        case .anteriorRightProximalLeg:
            return (-0.2,0.16)
        case .anteriorLeftProximalLeg:
            return (-0.05,0.16)
        case .posteriorRightDistalLeg:
            return (-0.63,0.5)
        case .posteriorLeftDistalLeg:
            return (-0.75,0.5)
        case .anteriorRightDistalLeg:
            return (-0.19,0.5)
        case .anteriorLeftDistalLeg:
            return (-0.07,0.5)
        }
    }
}
