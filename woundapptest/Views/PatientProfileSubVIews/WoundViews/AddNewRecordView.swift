//
//  AddNewRecordView.swift
//  woundapptest
//
//  Created by Brady Robshaw on 2/23/23.
//

import SwiftUI

struct AddNewRecordView: View {
    @EnvironmentObject var viewModel: PatientWoundVM
    @State private var record = WoundRecord.emptyForm
    
    @State private var perimeter = "" {
        didSet {
            record.perimeter = Double(perimeter) ?? 0
        }
    }
    @State private var depth = "" {
        didSet {
            record.perimeter = Double(perimeter) ?? 0
        }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    TextField("Stage", text: $record.stage)
                    TextField("Perimeter cm", text: $perimeter)
                        .keyboardType(.numberPad)
                    TextField("Depth cm", text: $depth)
                        .keyboardType(.numberPad)
                    
                    
                    Section("Notes") {
                        TextEditor(text: $record.notes)
                    }
                }
                
                Button {
                    viewModel.newRecord(record)
                } label: {
                    MediumButton(text: "Add New Record", fgcolor: "Primary", bgcolor: "Whght", width: 125, height: 45)
                }
            }
            .navigationTitle("New Record")
            .navigationBarTitleDisplayMode(.large)
        }
        
    }
}

struct AddNewRecordView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AddNewRecordView()
                .environmentObject(PatientWoundVM(patient: .example))
        }
        
    }
}
