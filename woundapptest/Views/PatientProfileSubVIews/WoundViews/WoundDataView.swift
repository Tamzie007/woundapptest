//
//  WoundDataView.swift
//  woundapptest
//
//  Created by Brady Robshaw on 2/22/23.
//

import SwiftUI

struct WoundDataView: View {
    @EnvironmentObject var viewModel: PatientWoundVM
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Location: \(viewModel.displayData.location)")
                Text("Date: \("Jan 23, 2023")")
                if viewModel.displayRecord == nil {
                    Text("Stage: \(viewModel.displayData.stageFormated)")
                    Text("Depth: \(viewModel.displayData.woundRecord.first?.depth ?? 0) cm")
                    Text("Perimeter: \(viewModel.displayData.woundRecord.first?.perimeter ?? 0) cm")
                } else {
                    Text("Stage: \(viewModel.displayRecord!.stage)")
                    Text("Depth: \(viewModel.displayRecord!.depth) cm")
                    Text("Perimeter: \(viewModel.displayRecord!.perimeter) cm")
                }
                
                
                Divider()
                
                ScrollView {
                    Text(viewModel.displayRecord == nil ? viewModel.displayData.notes : viewModel.displayRecord!.notes)
                }
            }
            .padding(.leading)
        }
        .padding(.bottom)
    }
}

struct WoundDataView_Previews: PreviewProvider {
    static var previews: some View {
        let vm = PatientWoundVM(patient: Patients.example)
        vm.setDisplay(to: Wound.example)
        
        return WoundDataView().environmentObject(vm)
    }
}
