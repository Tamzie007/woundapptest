//
//  AddNewWoundView.swift
//  woundapptest
//
//  Created by Brady Robshaw on 2/23/23.
//

import SwiftUI

struct AddNewWoundView: View {
    @EnvironmentObject var viewModel: PatientWoundVM
    @State private var newWound: Wound = Wound(uuid: UUID().uuidString, name: "", location: "", stage: "", notes: "", woundRecord: [])
    
    var body: some View {
        VStack {
            Form {
                TextField("Name", text: $newWound.name)
                TextField("Stage", text: $newWound.stage)
                
                Picker("Location", selection: $newWound.location) {
                    ForEach(LocationCode.allCases, id: \.id) { value in
                        Text(value.rawValue).tag(value.rawValue)
                    }
                }
                
                Section("Notes") {
                    TextEditor(text: $newWound.notes)
                }
                
            }
            
            Button {
                viewModel.newWound(newWound)
            } label: {
                MediumButton(text: "Add New Wound", fgcolor: "Primary", bgcolor: "Whght", width: 125, height: 45)
            }
        }
        .navigationTitle("New Wound")
    }
}

struct AddNewWoundView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AddNewWoundView()
        }
        
    }
}
