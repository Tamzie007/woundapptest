//
//  SepsisSurvey.swift
//  woundapptest
//
//  Created by stella on 3/2/22.
//

import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift
import SwiftUI

struct SepsisSurvey: View {
    
    @Binding var show:Bool
    @State var patient:Patients
    
    var body: some View {
        GeometryReader{ geo in
            ZStack(alignment: .center){
                if show{
                    Rectangle().foregroundColor(.white)
                    VStack{
                        SurveyQuestion(question: "1. Unexplained altered mental status, confusion or disorientation?", sepsisBool: $patient.sepsisSurvey.confusion)
                        SurveyQuestion(question: "2. Shorness of breath?", sepsisBool: $patient.sepsisSurvey.shortnessOfBreath)
                        SurveyQuestion(question: "3. Hypotension?", sepsisBool: $patient.sepsisSurvey.hypotension)
                        SurveyQuestion(question: "4. Signs of wound infection?", sepsisBool: $patient.sepsisSurvey.infection)
                        SurveyQuestion(question: "5. Heart rate > 140/min or < 40/min?", sepsisBool: $patient.sepsisSurvey.highHeartRate)
                        SurveyQuestion(question: "6. Extreme pain or discomfort?", sepsisBool: $patient.sepsisSurvey.pain)
                        SurveyQuestion(question: "7. Fever, leukocytosis, patient shivering or feeling very cold, clammy or sweathy skin?", sepsisBool: $patient.sepsisSurvey.fever)
                        HStack{
                            Spacer()
                            Button {
                                show = false
                                //opacity = 1.0
                            } label: {
                                Text("Cancel").foregroundColor(Color("Icon_Scanwound")).padding(8).overlay(
                                    RoundedRectangle(cornerRadius: 5)
                                        .stroke(Color("Icon_Scanwound"), lineWidth: 1)
                                )
                            }
                            Group {
                                Button {
                                    addSurvey()
                                } label: {
                                    Text("Add").foregroundColor(.white).padding(8)
                                }
                            }.background(Color("Icon_Scanwound")).cornerRadius(5).padding(5)
                            
                        }.padding(.horizontal)
                    }
                }
            }.frame(width: geo.size.width-20, height: geo.size.height-20, alignment: .center).fixedSize().position(x: geo.frame(in: .local).midX, y: geo.frame(in: .local).midY)
        //.fixedSize(horizontal: false, vertical: true)
        }
    }
}

extension SepsisSurvey {
    func addSurvey() {
        let db = Firestore.firestore()
        
        let data = try? Firestore.Encoder().encode(patient.sepsisSurvey)
        guard let data = data else { return }
        
        db.collection(FirebaseConstance.patients).document(patient.id!).updateData(["Sepsis Survey":data]) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                show = false
            }
        }
    }
}

struct SepsisSurvey_Previews: PreviewProvider {
    static var previews: some View {
        SepsisSurvey(show:.constant(true),patient: Patients.example)
    }
}
