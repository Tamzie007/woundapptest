//
//  BradenScore.swift
//  woundapptest
//
//  Created by stella on 3/4/22.
//

import SwiftUI

struct BradenScore: View {
    
    var totalScore: Int
    var bradenScores: BradenScoresModel
    @Binding var newScore:Bool
    @Binding var opacity:Double
    
    var body: some View {
        VStack(alignment:.center,spacing:10){
            Text(String(totalScore)).bold().font(.title).foregroundColor(Color("Icon_Scanwound"))
            BradenQuestion(question: "Sensory perception", mesg: "Completely limited", score: bradenScores.sensoryPerception)
            BradenQuestion(question: "Moisture", mesg: "Occasionally moist", score: bradenScores.moisture)
            BradenQuestion(question: "Activity", mesg: "Walks occasionally", score: bradenScores.activities)
            BradenQuestion(question: "Mobility", mesg: "No limitation", score: bradenScores.mobility)
            BradenQuestion(question: "Nutrition", mesg: "Probably inadequate", score: bradenScores.nutrition)
            BradenQuestion(question: "Friction and shear", mesg: "Potential problem", score: bradenScores.friction)
            Group{
                Button {
                    newScore.toggle()
                    opacity = 0.5
                } label: {
                    Text("Calculate new score").foregroundColor(.white)
                }
            }.padding(10).background(Color("Icon_Patients")).cornerRadius(5).padding(.vertical)

        }.padding()
    }
}

struct BradenScore_Previews: PreviewProvider {
    static var previews: some View {
        BradenScore(totalScore: 7, bradenScores: BradenScoresModel.example, newScore: .constant(false),opacity: .constant(1.0))
    }
}
