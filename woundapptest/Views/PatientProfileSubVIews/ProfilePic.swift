//
//  ProfilePic.swift
//  woundapptest
//
//  Created by Brady Robshaw on 1/13/23.
//

import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift
import SwiftUI

struct PatientPicturePicker: View {
    var patient: Patients
    @StateObject var viewmodel = ScanWoundVM()
    @EnvironmentObject var saveimagesvm: SaveImagesVM
    
    var body: some View {
        HStack(alignment: .firstTextBaseline) {
            //WebImage(url:viewmodel.url)
            if saveimagesvm.image == nil {
                Image("PaulSmith").resizable().frame(width: 130, height: 130).clipShape(Circle())
            } else {
                Image(uiImage: saveimagesvm.image!).resizable().frame(width: 130, height: 130).clipShape(Circle())
            }

            Button {
                viewmodel.source = .library
                viewmodel.showPhotoPicker()
            } label: {
                Image(systemName: "camera.fill")
                    .frame(alignment: .bottom)
                    .foregroundColor(.black)
            }
        }
        .onAppear {
            saveimagesvm.retrievePatientImage(for: patient.id)
        }
        .sheet(isPresented: $viewmodel.showImagePicker, onDismiss: {
            saveimagesvm.savePatientImage(for: patient.id)
        }) {
            ImagePicker(sourceType: viewmodel.source == .library ? .photoLibrary: .camera, image:$saveimagesvm.image)
                .ignoresSafeArea()
        }
        .alert("Error", isPresented: $viewmodel.showCameraAlert, presenting: viewmodel.cameraError, actions: {
            cameraError in
            cameraError.button
        }, message: { cameraError in
            Text(cameraError.message)
        })
    }
}

struct ProfilePic_Previews: PreviewProvider {
    static var previews: some View {
        PatientPicturePicker(patient: Patients.example)
            .environmentObject(SaveImagesVM())
    }
}
