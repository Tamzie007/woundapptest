//
//  PatientProfile.swift
//  woundapptest
//
//  Created by stella on 2/28/22.
//

import SwiftUI

struct PatientProfile: View {
    
    @State var opacity = 1.0
    @State var show:Bool = false
    @State var newScore:Bool = false
    var patient:Patients
    
    var body: some View {
        //NavigationView {
            ZStack {
                if (show || newScore){
                    Color.gray.ignoresSafeArea()
                }
                else{
                    Color.white.ignoresSafeArea()
                }
                ScrollView {
                    VStack{
                        Profile(patient:patient)
                        VStack(spacing:30){

                            ExpandInfo(information: "Contact Details") {
                                ContactDetails(patient: patient)
                            }
                            
                            ExpandInfo(information: "Patient History") {
                                PatientHistory(patient:patient)
                            }
                            
                            ExpandInfo(information: "Wounds") {
                                Wounds(patient:patient)
                                    .environmentObject(PatientWoundVM(patient: patient))
                            }
                            
                            ExpandInfo(information: "Braden Score") {
                                BradenScore(totalScore: patient.bradenScoreTotal, bradenScores: patient.bradenScore, newScore: $newScore,opacity:$opacity)
                            }
                            
                            ExpandInfo(information: "Daily Check-in") {
                                DailyCheckIn(show: $show)
                            }
                            
                            ExpandInfo(information: "Tests") {
                                Test()
                            }
                            
                            ExpandInfo(information: "Additional information") {
                                EmptyView()
                            }
                            
                            ExpandInfo(information: "Treatment") {
                                EmptyView()
                            }
                        }
                    }
                    
                }.opacity(opacity)
                SepsisSurvey(show: $show,patient: patient)
                BradenFormComplete(newScore: $newScore,opacity:$opacity)
            }.navigationBarTitle("",displayMode: .inline)
        //.navigationBarHidden(true)
        //}
    }
}
struct PatientProfile_Previews: PreviewProvider {
    static var previews: some View {
        PatientProfile(patient: Patients.testData[0])
            .environmentObject(SaveImagesVM())
    }
}
