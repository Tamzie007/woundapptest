//
//  FirebaseConstance.swift
//  woundapptest
//
//  Created by Brady Robshaw on 2/9/23.
//

import Foundation

struct FirebaseConstance {
    static let patients = "Synthetic_data"
    static let subWounds = "Wounds"
}
