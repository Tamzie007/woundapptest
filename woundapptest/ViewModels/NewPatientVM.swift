//
//  NewPatientVM.swift
//  woundapptest
//
//  Created by Brady Robshaw on 1/9/23.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class NewPatientVM : ObservableObject {
    @Published var showingAlert = false
    var alertText = "A first and last name is required to save a new patient."
    
    @Published var firstName: String = ""
    @Published var lastName: String = ""
    @Published var bd: Date = Date()
    @Published var sex: String = ""
    @Published var height: String = ""
    @Published var weight: String = ""
    @Published var address: String = ""
    @Published var phone: String = ""
    @Published var email: String = ""
    @Published var mrn: String = ""
    
    @Published var patientHistory: PatientHistoryModel = PatientHistoryModel.allfalse
    
    private var db = Firestore.firestore()
    
    private func addPatient(patient: Patients) {
        do{
            let _ = try db.collection(FirebaseConstance.patients).addDocument(from: patient)
        }
        catch{
            print(error)
        }
    }
    
    private func canBeSaved() -> Bool {
        if firstName.trimmingCharacters(in: .whitespacesAndNewlines) == "" || lastName.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            showingAlert = true
            return false
        }
        
        return true
    }
    
    func clearAll() {
        firstName = ""
        lastName = ""
        bd = Date()
        sex = ""
        height = ""
        weight = ""
        address = ""
        phone = ""
        email = ""
        mrn = ""
    }
    
    func save() -> Bool {
        guard canBeSaved() else { return false }
        
        let newPatient = Patients(firstName: firstName,
                                  lastName: lastName,
                                  middleName: "",
                                  gender: sex,
                                  height: Double(height) ?? 0,
                                  dateOfBirth: bd.description,
                                  MRN: Int(mrn) ?? 0,
                                  weight: Double(weight) ?? 0,
                                  phone: phone,
                                  email: email,
                                  address: address,
                                  emergencyContactName: "",
                                  emergencyContactRelationship: "",
                                  emergencyContactNumber: "",
                                  sepsisSurvey: SepsisSurveyModel.allFalse,
                                  bradenScoreTotal: BradenScoresModel.allFour.total,
                                  bradenScore: BradenScoresModel.allFour,
                                  patientHistory: patientHistory)
        
        addPatient(patient: newPatient)
        
        clearAll()
        return true
    }
}
