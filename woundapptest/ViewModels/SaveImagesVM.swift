//
//  SaveImagesVM.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 06/03/2022.
//

import Foundation
import FirebaseAuth
import Firebase

class SaveImagesVM: ObservableObject {
    let auth = Auth.auth()
    
    @Published var image: UIImage?
    
    //Saving Images to Firebase
    func persistImageToStorage() {
        guard let uid = auth.currentUser?.uid else {return}
        //let ref =  Storage.storage().reference(withPath: uid)
        let ref =  Storage.storage().reference()
        guard let imageData = self.image?.pngData() else {return}
        let tempName = ref.child("woundImages/file-\(uid).png")
        tempName.putData(imageData, metadata: nil) { metaData, error in
            if let error = error {
                print("Failed to push image to storage \(error.localizedDescription)")
                return
            }
            tempName.downloadURL {  url, err in
                if let err = err {
                    print(err)
                }
                print("successfully stored image \(url?.absoluteString ?? "")")
            }
        }
    }
    
    func savePatientImage(for patientID: String?) {
        guard let patientID = patientID else {
            print(" cannot save patient image to nill id")
            return
        }
        //TODO: limit the size of upload image
        // The quality of the resulting JPEG image, expressed as a value from 0.0 to 1.0. The value 0.0 represents the maximum compression (or lowest quality) while the value 1.0 represents the least compression (or best quality).
        guard let imageData = self.image?.jpegData(compressionQuality: 0.5) else { return }
        let ref =  Storage.storage().reference()
        let tempName = ref.child("patientsprofileimg/\(patientID).jpeg")
        tempName.putData(imageData, metadata: nil) { metaData, error in
            if let error = error {
                print("Failed to push image to storage \(error.localizedDescription)")
                return
            }
            tempName.downloadURL {  url, err in
                if let err = err {
                    print(err)
                }
                print("successfully stored image \(url?.absoluteString ?? "")")
            }
        }
    }
    
    //Saving Videos to Firebase
    func saveVideo(url: URL?) {
        guard let videoUrl = url else { return }
                
                let videoName = NSUUID().uuidString
                let storageRef = Storage.storage().reference().child("\(videoName).mov")
                storageRef.putFile(from: videoUrl as URL, metadata: nil) { (metaData, error) in
                     // IMPORTANT: this is where I got the error from
                    if error != nil {
                        print("error uploading video: \(error!.localizedDescription)")
                    } else {
                        // successfully uploaded the video
                        storageRef.downloadURL { (url, error) in
                            if error != nil {
                                print("error downloading uploaded videos Url: \(error!.localizedDescription)")
                            } else {
//                                if let downloadUrl = url {
//                                    let contentType = "videoUrl" //just a var for the following func
////                                    self.uploadPost(for: downloadUrl, contentType: contentType) // func that uploads the url to the database
//                                }
                            }
                        }
                    }
                }
    }
    
    func retrievePatientImage(for patientID: String?) {
        guard let patientID = patientID else { return }
        image = nil
        
        let storage = Storage.storage()
        
        let ref = storage.reference(withPath: "patientsprofileimg").child("\(patientID)"+".jpeg")
        //TODO: limit the size of download image, to do this, you have to compress upload image
        ref.getData(maxSize: 3_000_000) { data, error in
            if let error = error {
                print("error occurred during patient image retrieval: \(error.localizedDescription)")
            } else if let data = data{
                print("PatientImage bite size: \(data.count)")
                DispatchQueue.main.async {
                    self.image = UIImage(data: data)
                }
            }
        }
    }
}
