//
//  PatientWoundVM.swift
//  woundapptest
//
//  Created by Brady Robshaw on 1/23/23.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class PatientWoundVM: ObservableObject {
    let patient: Patients
    @Published var wounds: [Wound] = []
    @Published var displayData: Wound = Wound.error
    @Published var displayRecord: WoundRecord? = nil
    
    private var db = Firestore.firestore()
    
    init(patient: Patients) {
        self.patient = patient
    }
    
    //MARK: functions that pertain to affecting what information is displayed
    func setDisplay(to wound: Wound) {
        displayData = wound
        displayData.woundRecord.sort()
        displayData.woundRecord.reverse()
    }
    
    func showWoundRecord(_ record: WoundRecord?) {
        displayRecord = record
    }
    
    //MARK: functions that pertain to saving information to fire base
    func newWound(_ wound: Wound) {
        guard canSaveNewWound(wound) else { return }
        
        do {
            var ref: DocumentReference? = nil
            ref = try db.collection(FirebaseConstance.patients).document(patient.id!).collection(FirebaseConstance.subWounds).addDocument(from: wound) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                }
            }
        }
        catch {
            print("Error adding document: \(error)")
        }
    }
    
    private func canSaveNewWound(_ wound: Wound) -> Bool {
        if wound.name.trimmingCharacters(in: .whitespacesAndNewlines) == "" || wound.stage.trimmingCharacters(in: .whitespaces) == "" {
            return false
        } else {
            return true
        }
    }
    
    func newRecord(_ record: WoundRecord) {
        let ref = db.collection(FirebaseConstance.patients).document(patient.id!).collection(FirebaseConstance.subWounds).document(displayData.id!)
        
        ref.updateData([
            "Record" : FieldValue.arrayUnion([record])
        ])
    }
    
    //MARK: functions that pull down data from firebase
    func loadWounds() {
        db.collection(FirebaseConstance.patients).document(patient.id!).collection(FirebaseConstance.subWounds).addSnapshotListener { (QuerySnapshot, error) in
            guard let documents = QuerySnapshot?.documents else {
                print("No Sub-Wounds Documents")
                return
            }
            
            self.wounds = documents.compactMap { (QueryDocumentSnapshot)  -> Wound? in
                let b: Wound?
                //let id = QueryDocumentSnapshot.documentID
                do {
                    b = try QueryDocumentSnapshot.data(as: Wound.self)
                }
                catch {
                    print(error)
                    b = nil
                }
                
                //b?.id = id
                print(b)
                return b
            }
        }
    }
    
    
}
