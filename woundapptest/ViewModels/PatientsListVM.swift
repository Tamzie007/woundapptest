//
//  PatientsListVM.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 16/01/2022.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class PatientsListVM : ObservableObject{
    
    @Published var patients = [Patients]()
    @Published var url: URL?
    
    let storage = Storage.storage()
    
    private var db = Firestore.firestore()
    
    func fetchData(){
        db.collection(FirebaseConstance.patients).addSnapshotListener (){(QuerySnapshot, error) in
            guard let documents = QuerySnapshot?.documents else {
                print("No Documents")
                return
            }
            
            self.patients = documents.compactMap { (QueryDocumentSnapshot)  -> Patients? in
                //return try? QueryDocumentSnapshot.data(as: Patients.self)
                var data: Patients?
                let id = QueryDocumentSnapshot.documentID
                
                do {
                    data = try QueryDocumentSnapshot.data(as: Patients.self)
                    data?.id = id
                    print("Patients id: \(data?.id ?? "no id")")
                }
                catch {
                    print(error)
                    data = nil
                }
                return data
//            let data = QueryDocumentSnapshot.data()
//
//            let firstName = data["First Name"] as? String ?? ""
//            let lastName = data["Last Name"] as? String ?? ""
//            let middleName = data["Middle Name"] as? String ?? ""
//            let gender = data["Gender"] as? String ?? ""
//            let height = data["Height"] as? String ?? ""
//            let dateOfBirth = data["Birth Date"] as? String ?? ""
//            let weight = data["Weight"] as? String ?? ""
//
//            return Patients(firstName: firstName, lastName: lastName, middleName: middleName, gender: gender, height: height, dateOfBirth: dateOfBirth, weight: weight)
            }
        }
    }
    
    func fetchImage(id: String)-> URL?{
        let ref = storage.reference(withPath: "patientsprofileimg")
        print(ref)
        ref.child("\(id)"+".png").downloadURL{ (url, error) in
                        if let error = error {
                            print("\(error)")
                        }
                        
                        DispatchQueue.main.async {
                            self.url = url
                        }
        }
        return url
    }
}
