//
//  SepsisSurvey.swift
//  woundapptest
//
//  Created by Brady Robshaw on 1/26/23.
//

import Foundation

struct SepsisSurveyModel: Codable, Hashable {
    var confusion: Bool
    var shortnessOfBreath: Bool
    var hypotension: Bool
    var infection: Bool
    var highHeartRate: Bool
    var pain: Bool
    var fever: Bool
    
    enum CodingKeys: String, CodingKey {
        case confusion = "Confusion"
        case shortnessOfBreath = "Shortness of Breath"
        case hypotension = "Hypotension"
        case infection = "Infection"
        case highHeartRate = "High Heart Rate"
        case pain = "Pain"
        case fever = "Fever"
    }
    
    static let example = SepsisSurveyModel(confusion: false, shortnessOfBreath: true, hypotension: false, infection: false, highHeartRate: false, pain: true, fever: true)
    static let allFalse = SepsisSurveyModel(confusion: false, shortnessOfBreath: false, hypotension: false, infection: false, highHeartRate: false, pain: false, fever: false)
}

struct BradenScoresModel: Codable, Hashable {
    var sensoryPerception: Int
    var moisture: Int
    var activities: Int
    var mobility: Int
    var nutrition: Int
    var friction: Int
    
    var total: Int {
        sensoryPerception + moisture + activities + mobility + nutrition + friction
    }
    
    enum CodingKeys: String, CodingKey {
        case sensoryPerception = "Sensory Perception"
        case moisture = "Moisture"
        case activities = "Activities"
        case mobility = "Mobility"
        case nutrition = "Nutrition"
        case friction = "Friction"
    }
    
    static let example = BradenScoresModel(sensoryPerception: 2, moisture: 4, activities: 1, mobility: 3, nutrition: 4, friction: 4)
    static let allFour = BradenScoresModel(sensoryPerception: 4, moisture: 4, activities: 4, mobility: 4, nutrition: 4, friction: 4)
}

struct PatientHistoryModel: Codable, Hashable {
    var diabetes: Bool
    var highCholesterol: Bool
    var highBloodPressure: Bool
    var otherConditions: Bool
    var smoker: Bool
    var vaping: Bool
    var drinkingAlcohol: Bool
    var drugs: Bool
    var physicalActivity: Bool
    var maritalStatus: Bool
    
    enum CodingKeys: String, CodingKey {
        case diabetes = "Diabetes"
        case highCholesterol = "High Cholesterol"
        case highBloodPressure = "High Blood Pressure"
        case otherConditions = "Other Conditions"
        case smoker = "Smoker"
        case vaping = "Vaping"
        case drinkingAlcohol = "Drinking Alcohol"
        case drugs = "Drugs"
        case physicalActivity = "Physical Activity"
        case maritalStatus = "Marital Status"
    }
    
    static let example = PatientHistoryModel(diabetes: true, highCholesterol: false, highBloodPressure: false, otherConditions: true, smoker: false, vaping: true, drinkingAlcohol: true, drugs: false, physicalActivity: true, maritalStatus: false)
    static let allfalse = PatientHistoryModel(diabetes: false, highCholesterol: false, highBloodPressure: false, otherConditions: false, smoker: false, vaping: false, drinkingAlcohol: false, drugs: false, physicalActivity: false, maritalStatus: false)
}
