//
//  Patients.swift
//  woundapptest
//
//  Created by Oluwatamilore Adenipekun on 16/01/2022.
//

import Foundation
import FirebaseFirestoreSwift
import SwiftUI

struct Patients: Identifiable, Codable {
    @DocumentID  var id: String? //= UUID().uuidString
    var firstName: String
    var lastName: String
    var middleName: String
    var gender: String
    var height: Double
    var dateOfBirth: String // Figure out how to allow user to select from calender picker
    var MRN: Int
    var weight: Double
    var phone: String
    var email: String
    var address: String
    var emergencyContactName: String
    var emergencyContactRelationship: String
    var emergencyContactNumber: String
    var sepsisSurvey: SepsisSurveyModel
    var bradenScoreTotal: Int
    var bradenScore: BradenScoresModel
    var patientHistory: PatientHistoryModel
    
    var phoneString: String {
        String(phone)
    }
    var birthDateString: String {
        //dateOfBirth.formatted(date: .numeric, time: .omitted)
        dateOfBirth
    }
    var MRNString: String {
        String(MRN)
    }
    
    enum CodingKeys: String, CodingKey{
        case firstName = "First Name"
        case lastName = "Last Name"
        case middleName = "Middle Name"
        case gender = "Gender"
        case height = "Height"
        case dateOfBirth = "Birth Date"
        case MRN = "MRN"
        case weight = "Weight"
        case phone = "Phone"
        case email = "Email"
        case address = "Address"
        case emergencyContactName = "Emergency Contact Name"
        case emergencyContactRelationship = "Emergency Contact Relationship"
        case emergencyContactNumber = "Emergency Contact Number"
        case sepsisSurvey = "Sepsis Survey"
        case bradenScoreTotal = "Braden Score Total"
        case bradenScore = "Braden Scores"
        case patientHistory = "Patient History"
        //case wounds = "Wounds"
    }
    
    static let example = Patients(firstName: "Jane", lastName: "Doe", middleName: "", gender: "Female", height: 163.0, dateOfBirth: Date().description, MRN: 123456789, weight: 50.0, phone: "(202)555-0191", email: "janedoe22@gmail.com", address: "2971 Canis Heights Drive", emergencyContactName: "Paul Smith", emergencyContactRelationship: "Partner", emergencyContactNumber: "(292)555-0191", sepsisSurvey: SepsisSurveyModel.example, bradenScoreTotal: 15, bradenScore: BradenScoresModel.example, patientHistory: PatientHistoryModel.example)
    static let testData = [Patients.example]
}
