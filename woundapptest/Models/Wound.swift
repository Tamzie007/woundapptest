//
//  Wound.swift
//  woundapptest
//
//  Created by Carlos Morales III on 3/31/22.
//

import Foundation
import FirebaseFirestoreSwift
import Firebase

struct Wound: Identifiable, Codable, Hashable {
	@DocumentID var id: String?
    let uuid: String
	var name: String
    var location: String
    var stage: String
    var notes: String
    var woundRecord: [WoundRecord]
	
    var stageFormated: String {
        if stage.lowercased().hasPrefix("stage: ") {
            return String(stage.dropFirst(6))
        } else {
            return stage
        }
    }
    
    var locationCode: LocationCode? {
        LocationCode(rawValue: location)
    }
    
	enum CodingKeys: String, CodingKey{
		case id
        case uuid = "ID"
		case name = "Name"
        case location = "Location"
        case stage = "Stage"
        case notes = "Notes"
        case woundRecord = "Record"
	}
    
    static let example = Wound(uuid: UUID().uuidString,
                               name: "Pressure Ulcer",
                               location: "Anterior Right Abdomen",
                               stage: "2",
                               notes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                               woundRecord: [WoundRecord.example])
    static let error = Wound(uuid: UUID().uuidString,
                               name: "Error",
                               location: "Error",
                               stage: "Error",
                               notes: "Error",
                               woundRecord: [])
}

struct WoundRecord: Identifiable, Codable, Hashable, Comparable {
    var area: Double
    var depth: Double
    var id: String
    var imageIDs: DocumentReference
    var length: Double
    var notes : String
    var perimeter : Double
    var stage: String
    var UTC: Int
    var volume: Double
    var width: Double
    
    static func < (lhs: WoundRecord, rhs: WoundRecord) -> Bool {
        lhs.UTC < rhs.UTC
    }
    
    enum CodingKeys: String, CodingKey {
        case area = "Area"
        case depth = "Depth"
        case id = "ID"
        case imageIDs = "ImageIDs"
        case length = "Length"
        case notes = "Notes"
        case perimeter = "Perimeter"
        case stage = "Stage"
        case UTC = "UTC"
        case volume = "Volume"
        case width = "Width"
        
    }
    
    static let example = WoundRecord(area: 2.64,
                                     depth: 3.99,
                                     id: UUID().uuidString,
                                     imageIDs: Firestore.firestore().collection(FirebaseConstance.patients).document("13lqtjSFydwF0hoWodUy"),
                                     length: 1.96,
                                     notes: "lectus convallis est, vitae sodales nisi",
                                     perimeter: 5.04,
                                     stage: "Stage 1 Trauma hematoma",
                                     UTC: 1649783768,
                                     volume: 20.06,
                                     width: 5.13)
    
    static let emptyForm = WoundRecord(area: 0,
                                       depth: 0,
                                       id: UUID().uuidString,
                                       imageIDs: Firestore.firestore().collection(FirebaseConstance.patients).document("13lqtjSFydwF0hoWodUy"),
                                       length: 0,
                                       notes: "",
                                       perimeter: 0,
                                       stage: "",
                                       UTC: Int(Date().timeIntervalSince1970),
                                       volume: 0,
                                       width: 0)
}


enum LocationCode: String, CaseIterable, Identifiable {
    var id: Self {self}
    
    case anteriorHead = "Anterior Head"
    case posteriorHead = "Posterior Head"
    case anteriorRightChest = "Anterior Right Chest"
    case anteriorLeftChest = "Anterior Left Chest"
    case posteriorRightChest = "Posterior Right Chest"
    case posteriorLeftChest = "Posterior Left Chest"
    case anteriorRightAbdomen = "Anterior Right Abdomen"
    case anteriorLeftAbdomen = "Anterior Left Abdomen"
    case posteriorRightAbdomen = "Posterior Right Abdomen"
    case posteriorLeftAbdomen = "Posterior Left Abdomen"
    case gluteusRight = "Gluteus Right"
    case gluteusLeft = "Gluteus Left"
    case posteriorRightProximalArm = "Posterior Right Proximal Arm"
    case posteriorLeftProximalArm = "Posterior Left Proximal Arm"
    case anteriorRightProximalArm = "Anterior Right Proximal Arm"
    case anteriorLeftProximalArm = "Anterior Left Proximal Arm"
    case posteriorRightDistalArm = "Posterior Right Distal Arm"
    case posteriorLeftDistalArm = "Posterior Left Distal Arm"
    case anteriorRightDistalArm = "Anterior Right Distal Arm"
    case anteriorLeftDistalArm = "Anterior Left Distal Arm"
    case posteriorRightProximalLeg = "Posterior Right Proximal Leg"
    case posteriorLeftProximalLeg = "Posterior Left Proximal Leg"
    case anteriorRightProximalLeg = "Anterior Right Proximal Leg"
    case anteriorLeftProximalLeg = "Anterior Left Proximal Leg"
    case posteriorRightDistalLeg = "Posterior Right Distal Leg"
    case posteriorLeftDistalLeg = "Posterior Left Distal Leg"
    case anteriorRightDistalLeg = "Anterior Right Distal Leg"
    case anteriorLeftDistalLeg = "Anterior Left Distal Leg"
}
