
import SwiftUI

struct RecordVideoView: View {

  @State var isRecording = false
  var cameraView = CameraView()

  var body: some View {
    VStack {
      ZStack {
        cameraView
        VStack {
          HStack {
            Spacer()
            Button {
              cameraView.switchCamera()
            } label: {
              Image(systemName: "arrow.triangle.2.circlepath.camera")
                .padding()
                .foregroundColor(.white)
            }
          }
          Spacer()
          HStack {
            Spacer()
            Button {
              if !isRecording {
                cameraView.startRecording()
              } else {
                cameraView.stopRecording()
              }
              isRecording.toggle()
            } label: {
              Image(systemName: "record.circle")
                .font(.system(size: 60))
                .foregroundColor(isRecording ? Color.red : Color.white)
            }
            Spacer()
          }
        }
      }
    }
  }
}

struct RecordVideoView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
